package com.onvif.db.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.onvif.db.entity.CameraEntity;
import com.onvif.db.service.CameraService;

@Service
public class CameraServiceImpl implements CameraService {

	@Autowired
	RestTemplate restTemplate;
	@Value("${control.server-ip}")
	String controlIp;
	@Value("${control.api-port}")
	Integer controlApiPort;
	
	@Override
	public CameraEntity findOne(CameraEntity cameraEntity) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceId", cameraEntity.getDeviceId());
		params.put("channelId", cameraEntity.getChannelId());
		String result = restTemplate.getForObject("http://"+controlIp+":"+controlApiPort+"/media/findOne?deviceId={deviceId}&channelId={channelId}", String.class,params);
		JSONObject jsonObj = JSONObject.parseObject(result);
		Integer code = jsonObj.getInteger("code");
		if(code == 200) {
			CameraEntity c = JSON.parseObject(jsonObj.getString("data"), new TypeReference<CameraEntity>() {});
			return c;
		} else {
			return null;
		}
	}

}
