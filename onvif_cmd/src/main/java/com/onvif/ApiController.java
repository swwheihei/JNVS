package com.onvif;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onvif.commons.ApiCodeEnum;
import com.onvif.commons.ApiResult;

@RestController
@RequestMapping("/onvif")
public class ApiController {
	@Autowired
	SendOnvifCmd sendOnvifCmd;
	
	@GetMapping("/live/{deviceId}_{channelId}")
	public ApiResult live(@PathVariable String deviceId,@PathVariable String channelId) {
		String rtspUrl = sendOnvifCmd.stream(deviceId, channelId);
		if(rtspUrl!=null) {
			return new ApiResult(ApiCodeEnum.OK,(Object)rtspUrl);
		} else {
			return new ApiResult(ApiCodeEnum.INVALID_REQUEST);
		}
	}
	@GetMapping("/ptz/{deviceId}_{channelId}")
	public ApiResult ptz(@PathVariable String deviceId,@PathVariable String channelId,Double leftRight, Double upDown, Double inOut){
		Boolean ptzResult = sendOnvifCmd.ptz(deviceId, channelId, leftRight, upDown, inOut);
		if(ptzResult) {
			return new ApiResult(ApiCodeEnum.OK);
		} else {
			return new ApiResult(ApiCodeEnum.INVALID_REQUEST);
		}
	}
}
