package com.gb28181.commons;
public enum ApiCodeEnum {
	OK(200,"成功"),
	SERVER_ERROR(500,"服务器内部错误"),
	INVALID_REQUEST(400,"参数错误"),
	UNAUTHORIZED (401,"无访问权限"),
	NOT_FOUND(404,"访问路径不正确"),
	NOT_LOGIN(403,"未登录"),
	;
	
	private Integer code;
	private String msg;
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	ApiCodeEnum(Integer code,String msg){
		this.code=code;
		this.msg=msg;
	}
}
