package com.control.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.control.db.entity.CameraEntity;

@Repository
public interface CameraRepository extends JpaRepository<CameraEntity, Integer>{
}
