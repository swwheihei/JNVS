package com.control.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.control.db.entity.BrandEntity;
import com.control.db.entity.CameraEntity;

@Repository
public interface BrandRepository extends JpaRepository<BrandEntity, Integer>{
}
