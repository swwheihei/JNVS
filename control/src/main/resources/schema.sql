create table t_brand (
    id bigint generated by default as identity,
    name varchar(255),
    rtsp_url varchar(255),
    primary key (id)
);
create table t_camera (
    id bigint generated by default as identity,
    brand_id integer,
    channel_id varchar(255),
    device_id varchar(255),
    ip varchar(255),
    live_key varchar(255),
    live_status integer,
    password varchar(255),
    record_status integer,
    rtsp_channel varchar(255),
    rtsp_port integer,
    status integer,
    type integer,
    username varchar(255),
    onvif_username varchar(255),
    onvif_password varchar(255),
    onvif_port integer,
    primary key (id)
);
create table t_record (
    id bigint generated by default as identity,
    channel_id varchar(255),
    device_id varchar(255),
    end_time integer,
    file_name varchar(255),
    file_path varchar(255),
    file_size integer,
    folder varchar(255),
    start_time integer,
    time_len integer,
    url varchar(255),
    primary key (id)
);